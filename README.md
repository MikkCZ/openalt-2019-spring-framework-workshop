# Ukázková aplikace pro workshop na OpenAltu 2019

Tato aplikace vznikla jako ukázka základní služby pro workshop na konferenci OpenAlt 2019 v Brně. Její jedinou funkcionalitou je omezené vytváření a výpis registrací přes REST nebo webovou stránku.

## Spuštění

### Pro ruční testování
```bash
mvn spring-boot:run
```

### Pro nasazení "do produkce"
```bash
mvn clean package
./target/openalt-workshop-0.0.1-SNAPSHOT.jar
```

### Explicitně s HSQLDB
```bash
mvn spring-boot:run -Dspring.profiles.active='hsqldb'
```

### Explicitně s mockem databáze
```bash
mvn spring-boot:run -Dspring.profiles.active='memory'
```

## Příklady použití

### Vytvoření registrace přes REST
```bash
curl -X POST http://localhost:8080/registrations/ -H 'Content-Type: application/json' --data '{"email":"chuck.norris@example.com", "personName":"Chuck", "personSurname":"Norris"}'
```

### Vytvoření registrace přes web
Formulář na [http://localhost:8080/registrations/](http://localhost:8080/registrations/).

### Výpis všech registrací přes REST
```bash
curl -X GET http://localhost:8080/registrations/
```

### Výpis všech registrací přes web
Tabulka na [http://localhost:8080/registrations/](http://localhost:8080/registrations/).
