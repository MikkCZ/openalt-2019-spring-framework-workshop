package com.example.openaltworkshop.registrations.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Registration {

    private int id;

    private String personName;
    private String personSurname;
    private String email;
}
