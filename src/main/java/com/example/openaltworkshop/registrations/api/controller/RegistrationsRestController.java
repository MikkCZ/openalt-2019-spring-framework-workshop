package com.example.openaltworkshop.registrations.api.controller;

import com.example.openaltworkshop.registrations.api.dto.Registration;
import com.example.openaltworkshop.registrations.service.RegistrationsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping(path = "/registrations")
@RequiredArgsConstructor
public class RegistrationsRestController {

    private final RegistrationsService service;

    @PostMapping
    public void createRegistration(@RequestBody Registration registration) {
        service.save(registration);
    }

    @GetMapping
    public Collection<Registration> retrieveAllRegistrations() {
        return service.getAll();
    }
}
