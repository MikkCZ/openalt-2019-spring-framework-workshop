package com.example.openaltworkshop.registrations.service;

import com.example.openaltworkshop.registrations.persistence.RegistrationEntity;
import com.example.openaltworkshop.registrations.api.dto.Registration;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class RegistrationsService {

    private final CrudRepository<RegistrationEntity, Integer> repository;

    public void save(Registration registration) {
        repository.save(dtoToEntity(registration));
    }

    public Collection<Registration> getAll() {
        return StreamSupport.stream(repository.findAll().spliterator(), false)
                .map(RegistrationsService::entityToDto)
                .collect(toList());
    }

    private static RegistrationEntity dtoToEntity(Registration dto) {
        RegistrationEntity entity = new RegistrationEntity();
        entity.setPersonName(dto.getPersonName());
        entity.setPersonSurname(dto.getPersonSurname());
        entity.setEmail(dto.getEmail());
        return entity;
    }

    private static Registration entityToDto(RegistrationEntity entity) {
        return new Registration(
                entity.getId(),
                entity.getPersonName(),
                entity.getPersonSurname(),
                entity.getEmail()
        );
    }
}
