package com.example.openaltworkshop.registrations.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "registrations")
@NoArgsConstructor
@Data
public class RegistrationEntity {

    @Id
    @GeneratedValue
    private int id;

    private String personName;
    private String personSurname;
    private String email;
}
