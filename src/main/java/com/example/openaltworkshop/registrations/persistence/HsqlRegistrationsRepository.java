package com.example.openaltworkshop.registrations.persistence;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("hsqldb")
public interface HsqlRegistrationsRepository extends CrudRepository<RegistrationEntity, Integer> {
}
