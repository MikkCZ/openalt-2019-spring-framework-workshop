package com.example.openaltworkshop.registrations.persistence;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Repository
@Profile("memory")
public class CollectionRegistrationsRepository implements CrudRepository<RegistrationEntity, Integer> {

    private final Collection<RegistrationEntity> registrations = new ArrayList<>();

    @Override
    public <S extends RegistrationEntity> S save(S entity) {
        registrations.add(entity);
        return entity;
    }

    @Override
    public Iterable<RegistrationEntity> findAll() {
        return registrations;
    }

    // for this simple service, these below do not require to be implemented

    @Override
    public <S extends RegistrationEntity> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<RegistrationEntity> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public Iterable<RegistrationEntity> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(RegistrationEntity entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends RegistrationEntity> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
