package com.example.openaltworkshop.registrations.web.controller;

import com.example.openaltworkshop.registrations.api.dto.Registration;
import com.example.openaltworkshop.registrations.service.RegistrationsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/registrations")
@RequiredArgsConstructor
public class RegistrationsWebController {

    private final RegistrationsService service;

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String register(Registration registration, Model model) {
        service.save(registration);
        return getAll(model);
    }

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String getAll(Model model) {
        model.addAttribute("registration", new Registration(0, "", "", ""));
        model.addAttribute("registrations", service.getAll());
        return "index";
    }
}
