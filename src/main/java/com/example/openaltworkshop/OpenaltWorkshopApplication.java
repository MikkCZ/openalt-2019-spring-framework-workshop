package com.example.openaltworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenaltWorkshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenaltWorkshopApplication.class, args);
	}

}
