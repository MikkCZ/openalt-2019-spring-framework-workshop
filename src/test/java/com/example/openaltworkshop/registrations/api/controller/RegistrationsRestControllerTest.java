package com.example.openaltworkshop.registrations.api.controller;

import com.example.openaltworkshop.registrations.api.dto.Registration;
import com.example.openaltworkshop.registrations.service.RegistrationsService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RegistrationsRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RegistrationsService registrationsService;

    @Test
    public void createRegistration() throws Exception {
        Registration registrationToCreate = new Registration(42, "Chuck", "Norris", "chuck.norris@example.com");

        mockMvc.perform(
                post("/registrations")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(objectMapper.writeValueAsString(registrationToCreate))
        )
                .andExpect(status().isOk());

        verify(registrationsService).save(registrationToCreate);
    }

    @Test
    public void retrieveAllRegistrations() throws Exception {
        Registration registrationToRetrieve = new Registration(42, "Chuck", "Norris", "chuck.norris@example.com");
        when(registrationsService.getAll()).thenReturn(Collections.singletonList(registrationToRetrieve));

        MockHttpServletResponse response = mockMvc.perform(
                get("/registrations")
                        .accept(MediaType.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andReturn().getResponse();

        assertThat(objectMapper.<List<Registration>>readValue(
                response.getContentAsString(),
                new TypeReference<List<Registration>>() {
                }
        ))
                .hasSize(1)
                .contains(registrationToRetrieve);
    }
}
